﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.Player
{
    public class PlayerControl : MonoBehaviour
    {
        private const string HORIZONTAL = "Horizontal";
        private const string VERTICAL = "Vertical";

        private float horizantalinput;
        private float verticalinput;

        private float currentsteeringangle;
        private float currentbreakforce;

        private bool isbreaking;

        [Header("Propoty")]
        public float motorforce;
        public float breakf;
        public float maxsteeringangle;

        [Header("colliders")]
        public WheelCollider Mwheelfon;
        public WheelCollider Mwheelback;
        public Transform Mwhelfon;
        public Transform Mwhelbac;

        void FixedUpdate()
        {
            GetInput();
            Handlemotor();
            HandleSteerong();
        }

        private void HandleSteerong()
        {
            currentsteeringangle = maxsteeringangle * horizantalinput;
            Mwheelfon.steerAngle = currentsteeringangle;
        }

        private void Handlemotor()
        {
            Mwheelfon.motorTorque = verticalinput * motorforce;
            currentbreakforce = isbreaking ? breakf : 0f;
            ApplyBreaking();
        }

        private void ApplyBreaking()
        {
            Mwheelback.brakeTorque = currentbreakforce;
            Mwheelback.brakeTorque = currentbreakforce;
        }

        private void GetInput()
        {
            horizantalinput = Input.GetAxis(HORIZONTAL);
            verticalinput = Input.GetAxis(VERTICAL);

            isbreaking = Input.GetKey(KeyCode.Space);
        }
    }
}
