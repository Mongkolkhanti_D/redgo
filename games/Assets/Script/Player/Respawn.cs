﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.Player
{
    class Respawn : MonoBehaviour
    {
        [Header("PLAYER")]
        [SerializeField] GameObject player;

        [Header("CHACK")]
        [SerializeField] List<GameObject> checkpoint;
        [SerializeField] Vector3 vectorPoint;

        void Update()
        {
            if (Input.GetKey(KeyCode.R))
            {
                player.transform.position = vectorPoint;
                player.transform.rotation = Quaternion.identity;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            vectorPoint = player.transform.position;
        }
    }
}
