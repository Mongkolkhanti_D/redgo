﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Script.Player.Interac
{
    public class ActorTriggerHandlerV2 : ActorTriggerHandler
    {
        public virtual IInteractable[] GetInteractables()
        {
            m_TriggeredGameObjects.RemoveAll(gameject => gameject == null);

            if (m_TriggeredGameObjects.Count == 0)
            {
                return null;
            }

            return m_TriggeredGameObjects[0].GetComponents<IInteractable>();
        }
    }
}
