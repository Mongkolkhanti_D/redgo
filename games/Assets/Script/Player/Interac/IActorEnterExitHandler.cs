﻿using UnityEngine;

namespace Assets.Script.Player.Interac
{
    public interface IActorEnterExitHandler
    {
        void ActorEnter(GameObject actor);
        void ActorExit(GameObject actor);
    }
}
