﻿using UnityEngine;

namespace Assets.Script.Player.Interac
{
    public interface IInteractable
    {
        void Interact(GameObject actor);
    }
}
