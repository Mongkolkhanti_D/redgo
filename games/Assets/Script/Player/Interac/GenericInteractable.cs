﻿using UnityEngine;
using UnityEngine.Events;

namespace Assets.Script.Player.Interac
{
    public class GenericInteractable : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected UnityEvent m_OnInteract = new();
        [SerializeField] protected UnityEvent m_OnActorEnter = new();
        [SerializeField] protected UnityEvent m_OnActorExit = new();


        public virtual void Interact(GameObject actor)
        {
            if (m_OnInteract != null)
                m_OnInteract.Invoke();

        }

        public virtual void ActorEnter(GameObject actor)
        {
            if (m_OnActorEnter != null)
                m_OnActorEnter.Invoke();

        }

        public virtual void ActorExit(GameObject actor)
        {
            if (m_OnActorExit != null)
                m_OnActorExit.Invoke();

        }

    }
}
