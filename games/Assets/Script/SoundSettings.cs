﻿using UnityEngine;
using UnityEngine.Audio;

namespace Assets.Script
{
    [CreateAssetMenu(menuName = "SoundSettingPreset",fileName ="SoundPreset")]
   public class SoundSettings : ScriptableObject
   {
        public AudioMixer audioMixer;

        [Header("MasterVolume")]
        public string MasterVolumeName = "MasterVolume";
        [Range(-80, 20)]
        public float MasterVolume;
   }
}
