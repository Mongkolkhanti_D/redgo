﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Script
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] protected SoundSettings m_soundSettings;

        public Slider m_SliderMasterVolume;

        void Start()
        {
            InitialiseVolume();
        }

        private void InitialiseVolume()
        {
            SetMasterVolume(m_soundSettings.MasterVolume);
        }

        public void SetMasterVolume(float vol)
        {
            m_soundSettings.audioMixer.SetFloat(m_soundSettings.MasterVolumeName, vol);

            m_soundSettings.MasterVolume = vol;
            m_SliderMasterVolume.value = m_soundSettings.MasterVolume;
        }
    }
}
