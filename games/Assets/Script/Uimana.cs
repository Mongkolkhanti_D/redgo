using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Uimana : MonoBehaviour
{
    [SerializeField] GameObject Q_image;
    [SerializeField] GameObject B_image;
    [SerializeField] GameObject G_image;
    void Update()
    {
        if(Q_image.activeInHierarchy == false)
        {
            G_image.SetActive(true);
        }
    }
}
