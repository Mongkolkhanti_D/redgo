﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Assets.Script.Racing
{
    public class TimeRacing : MonoBehaviour
    {
        public static int MinCount;
        public static int SecCount;
        public static float MilCount;
        public static string MilDisplay;

        public GameObject MinBox;
        public GameObject SecBox;
        public GameObject MilBox;

        public GameObject winPanel;

        void Update()
        {
            MilCount += Time.deltaTime * 10;
            MilDisplay = MilCount.ToString("F0");
            MilBox.GetComponent<TextMeshProUGUI>().text = "" + MilDisplay;

            if (MilCount >= 10)
            {
                MilCount = 0;
                SecCount += 1;
            }

            if(SecCount <= 9)
            {
                SecBox.GetComponent<TextMeshProUGUI>().text = "0" + SecCount + ".";
            }
            else
            {
                SecBox.GetComponent<TextMeshProUGUI>().text = "" + SecCount + ".";
            }

            if (SecCount >= 60)
            {
                SecCount = 0;
                MinCount += 1;
            }

            if (MinCount <= 9)
            {
                MinBox.GetComponent<TextMeshProUGUI>().text = "0" + MinCount + ":";
            }
            else
            {
                MinBox.GetComponent<TextMeshProUGUI>().text = "" + MinCount + ":";
            }

            if (winPanel.activeInHierarchy)
            {
                Time.timeScale = 0f;
            }
        }
    }
}
