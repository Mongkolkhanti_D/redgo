﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Assets.Script.Racing
{
    public class lapManager : MonoBehaviour
    {
        public GameObject LapComTrig;
        public GameObject HalfTrig;

        public GameObject lapsCount;
        public int lapsdone;

        public GameObject winPanel;
        public GameObject lapsGoal;

        private void OnTriggerEnter(Collider other)
        {
            lapsdone += 1;

            if(lapsdone == 2)
            {
                lapsGoal.SetActive(true);
            }

            if (lapsdone > 2)
            {
                winPanel.SetActive(true);
            }

            HalfTrig.SetActive(true);
            LapComTrig.SetActive(false);

            lapsCount.GetComponent<TextMeshProUGUI>().text = "" + lapsdone;
        }
    }
}
