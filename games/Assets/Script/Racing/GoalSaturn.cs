﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Assets.Script.Racing
{
    public class GoalSaturn : MonoBehaviour
    {
        public GameObject lapsgoal;
        public GameObject goal;

        private void OnTriggerEnter(Collider other)
        {
            lapsgoal.SetActive(false);
            goal.SetActive(true);

        }
    }
}
