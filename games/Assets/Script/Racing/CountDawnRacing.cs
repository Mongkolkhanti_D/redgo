﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Assets.Script.Racing
{
    public class CountDawnRacing : MonoBehaviour
    {
        public GameObject Countdawn;
        public AudioSource Go;
        public AudioSource Ready;
        public GameObject timer;
        public GameObject Player;
        public GameObject sound;

        private void Start()
        {
            StartCoroutine(CountStart());
        }

        IEnumerator CountStart()
        {
            yield return new WaitForSeconds(0.5f);
            Countdawn.GetComponent<TextMeshProUGUI>().text = "3";
            Ready.Play();
            Countdawn.SetActive(true);
            yield return new WaitForSeconds(1);
            Countdawn.SetActive(false);
            Countdawn.GetComponent<TextMeshProUGUI>().text = "2";
            Ready.Play();
            Countdawn.SetActive(true);
            yield return new WaitForSeconds(1);
            Countdawn.SetActive(false);
            Countdawn.GetComponent<TextMeshProUGUI>().text = "1";
            Ready.Play();
            Countdawn.SetActive(true);
            yield return new WaitForSeconds(1);
            Countdawn.SetActive(false);
            Go.Play();
            timer.SetActive(true);
            Player.SetActive(true);
            sound.SetActive(true);
        }
    }
}
