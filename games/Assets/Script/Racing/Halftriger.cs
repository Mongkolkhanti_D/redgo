﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Script.Racing
{
    public class Halftriger : MonoBehaviour
    {
        public GameObject lapComTrig;
        public GameObject halfTrg;

        private void OnTriggerEnter(Collider other)
        {
            lapComTrig.SetActive(true);
            halfTrg.SetActive(false);
        }
    }
}
