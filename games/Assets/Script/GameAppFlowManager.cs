﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Script
{
    public class GameAppFlowManager : MonoBehaviour
    {
        protected static bool IsSceneOptionLoaded;

        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }

        public void UnloadScene(string sceneName)
        {
            SceneManager.UnloadSceneAsync(sceneName);
        }

        public void LoadSceneAdditive(string sceneName)
        {
            SceneManager.LoadScene(sceneName,LoadSceneMode.Additive);
        }

        public void LoadOptionsScene(string optionSceneName)
        {
            if (!IsSceneOptionLoaded)
            {
                SceneManager.UnloadSceneAsync(optionSceneName);
                IsSceneOptionLoaded = false;
            }
        }

        public void ExitGame()
        {
            Application.Quit();
        }

        private void OnEnable()
        {
            SceneManager.sceneUnloaded += SceneUnloadEventHandler;
            SceneManager.sceneLoaded += SceneLoadEventHandler;
        }

        private void OnDisable()
        {
            SceneManager.sceneUnloaded -= SceneUnloadEventHandler;
            SceneManager.sceneLoaded -= SceneLoadEventHandler;
        }


        private void SceneUnloadEventHandler(Scene scene)
        {

        }

        private void SceneLoadEventHandler(Scene scene, LoadSceneMode mode)
        {
            if (scene.name.CompareTo("SceneOptions") != 0)
            {
                IsSceneOptionLoaded = false;
            }
        }
    }
}
