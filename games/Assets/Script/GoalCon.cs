using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalCon : MonoBehaviour
{
    public GameObject GoalTrig;
    public GameObject i_image;

    public GameObject winpanel;

    private void OnTriggerEnter(Collider other)
    {
        if (i_image.activeInHierarchy)
        {
            winpanel.SetActive(true);
            Time.timeScale = 0f;
        }
        else
        {

        }
    }
}
