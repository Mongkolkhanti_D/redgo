﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Assets.Script.Waypoints
{
#if UNITY_EDITOR
    public class Waypointmanager : EditorWindow
    {
        [MenuItem("Tools/Waypoint Editor")]
        public static void Open()
        {
            GetWindow<Waypointmanager>();
        }

        public Transform waypointRoot;

        private void OnGUI()
        {
            SerializedObject obj = new SerializedObject(this);

            EditorGUILayout.PropertyField(obj.FindProperty("waypointRoot"));

            if (waypointRoot == null)
            {
                EditorGUILayout.HelpBox("Root transform must be selected. Please assign a root transform.", MessageType.Warning);
            }
            else
            {
                EditorGUILayout.BeginVertical("box");
                DrawButtons();
                EditorGUILayout.EndVertical();
            }

            obj.ApplyModifiedProperties();
        }

        void DrawButtons()
        {
            if (GUILayout.Button("Create Waypoint"))
            {
                CreateWaypoint();
            }
            if (Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<Waypoints>())
            {
                if (GUILayout.Button("Create Waypoint Before"))
                {
                    CreateWaypointBefore();
                }
                if (GUILayout.Button("Create Waypoint After"))
                {
                    CreateWaypointAfter();
                }
                if (GUILayout.Button("Remove Waypoint"))
                {
                    RemoveWaypoint();
                }
            }
        }

        void CreateWaypoint()
        {
            GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoints));
            waypointObject.transform.SetParent(waypointRoot, false);

            Waypoints waypoint = waypointObject.GetComponent<Waypoints>();
            if (waypointRoot.childCount > 1)
            {
                waypoint.previousWaypoint = waypointRoot.GetChild(waypointRoot.childCount - 2).GetComponent<Waypoints>();
                waypoint.previousWaypoint.nextWaypoint = waypoint;
                //Place the waypoint at the last position
                waypoint.transform.position = waypoint.previousWaypoint.transform.position;
                waypoint.transform.forward = waypoint.previousWaypoint.transform.forward;
            }

            Selection.activeGameObject = waypoint.gameObject;
        }

        void CreateWaypointBefore()
        {
            GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoints));
            waypointObject.transform.SetParent(waypointRoot, false);

            Waypoints newWaypoint = waypointObject.GetComponent<Waypoints>();

            Waypoints selectedWaypoint = Selection.activeGameObject.GetComponent<Waypoints>();

            waypointObject.transform.position = selectedWaypoint.transform.position;
            waypointObject.transform.forward = selectedWaypoint.transform.forward;

            if (selectedWaypoint.previousWaypoint != null)
            {
                newWaypoint.previousWaypoint = selectedWaypoint.previousWaypoint;
                selectedWaypoint.previousWaypoint.nextWaypoint = newWaypoint;
            }

            newWaypoint.nextWaypoint = selectedWaypoint;

            selectedWaypoint.previousWaypoint = newWaypoint;

            newWaypoint.transform.SetSiblingIndex(selectedWaypoint.transform.GetSiblingIndex());

            Selection.activeGameObject = newWaypoint.gameObject;
        }

        void CreateWaypointAfter()
        {
            GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoints));
            waypointObject.transform.SetParent(waypointRoot, false);

            Waypoints newWaypoint = waypointObject.GetComponent<Waypoints>();

            Waypoints selectedWaypoint = Selection.activeGameObject.GetComponent<Waypoints>();

            waypointObject.transform.position = selectedWaypoint.transform.position;
            waypointObject.transform.forward = selectedWaypoint.transform.forward;

            newWaypoint.previousWaypoint = selectedWaypoint;

            if (selectedWaypoint.nextWaypoint != null)
            {
                selectedWaypoint.nextWaypoint.previousWaypoint = newWaypoint;
                newWaypoint.nextWaypoint = selectedWaypoint.nextWaypoint;
            }

            selectedWaypoint.nextWaypoint = newWaypoint;

            newWaypoint.transform.SetSiblingIndex(selectedWaypoint.transform.GetSiblingIndex());

            Selection.activeGameObject = newWaypoint.gameObject;
        }

        void RemoveWaypoint()
        {
            Waypoints selectedWapoint = Selection.activeGameObject.GetComponent<Waypoints>();

            if (selectedWapoint.nextWaypoint != null)
            {
                selectedWapoint.nextWaypoint.previousWaypoint = selectedWapoint.previousWaypoint;
            }
            if (selectedWapoint.previousWaypoint != null)
            {
                selectedWapoint.previousWaypoint.nextWaypoint = selectedWapoint.nextWaypoint;
                Selection.activeGameObject = selectedWapoint.previousWaypoint.gameObject;
            }

            DestroyImmediate(selectedWapoint.gameObject);
        }
    }
#endif
}
