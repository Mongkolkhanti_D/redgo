﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.Script.Waypoints
{
    class Waypoints : MonoBehaviour
    {
        public Waypoints previousWaypoint;
        public Waypoints nextWaypoint;

        [Range(0f, 15f)]
        public float width = 1f;

        public Vector3 GetPosition()
        {
            Vector3 minBound = transform.position + transform.right * width / 2f;
            Vector3 maxBound = transform.position - transform.right * width / 2f;

            return Vector3.Lerp(minBound, maxBound, Random.Range(0f, 1f));
        }
    }
}
