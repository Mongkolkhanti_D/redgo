﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.Waypoints
{
    class WaypointNavigator : MonoBehaviour
    {
        AiNavi controller;
        public Waypoints currentWaypoint;

        private void Awake()
        {
            controller = GetComponent<AiNavi>();
        }
        // Start is called before the first frame update
        void Start()
        {
            controller.SetDestination(currentWaypoint.GetPosition());
        }

        // Update is called once per frame
        void Update()
        {
            if (controller.reachedDestination)
            {
                currentWaypoint = currentWaypoint.nextWaypoint;
                controller.SetDestination(currentWaypoint.GetPosition());
            }
        }
    }
}
