﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.Waypoints
{
    class AiSpawner : MonoBehaviour
    {
        public GameObject pedestrianPrefab;
        public int pedestriansToSpawn;

        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(Spawn());
        }

        IEnumerator Spawn()
        {
            int count = 0;
            while (count < pedestriansToSpawn)
            {
                GameObject obj = Instantiate(pedestrianPrefab);
                Transform child = transform.GetChild(Random.Range(0, transform.childCount - 1));
                obj.GetComponent<WaypointNavigator>().currentWaypoint = child.GetComponent<Waypoints>();
                obj.transform.position = child.position;

                yield return new WaitForEndOfFrame();

                count++;
            }
        }
    }
}
