﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Assets.Script
{
    public class Times : MonoBehaviour
    {
        [Header("Component")]
        public TextMeshProUGUI timertext;
        public TextMeshProUGUI timerEnd;

        [Header("time")]
        public float currentTime;

        [Header("Star")]
        public GameObject star2;
        public GameObject star3;
        public GameObject star4;
        public GameObject star5;

        [Header("Pasuse")]
        public GameObject pausePanel;

        private void FixedUpdate()
        {
            currentTime = currentTime += Time.deltaTime;

            if (currentTime > 0)
            {
                int min = (int)currentTime / 60;
                int sec = (int)currentTime % 60;

                timertext.text = string.Format("{0:00}:{1:00}", min, sec);
                timerEnd.text = string.Format("{0:00}:{1:00}", min, sec);
            }

            if(currentTime > 50)
            {
                star5.SetActive(false);
            }
            if(currentTime > 60)
            {
                star5.SetActive(false);
                star4.SetActive(false);
            }
            if (currentTime > 70)
            {
                star5.SetActive(false);
                star4.SetActive(false);
                star3.SetActive(false);
            }
            if (currentTime > 80)
            {
                star5.SetActive(false);
                star4.SetActive(false);
                star3.SetActive(false);
                star2.SetActive(false);
            }

        }

        public void Pause()
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0f;
        }

        public void Resume()
        {
            pausePanel.SetActive(false);
            Time.timeScale = 1;
        }
    }
}
